import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

/// Message route arguments.
class MessageArguments {
  /// The RemoteMessage
  final RemoteMessage message;

  /// Whether this message caused the application to open.
  final bool openedApplication;

  // ignore: public_member_api_docs
  MessageArguments(this.message, this.openedApplication)
      : assert(message != null);
}

// Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
//   await Firebase.initializeApp();
//   print('Handling a background message ${message.messageId}');
// }

class PushReceiver extends StatefulWidget {
  const PushReceiver({Key? key}) : super(key: key);

  @override
  _PushReceiverState createState() => _PushReceiverState();
}

class _PushReceiverState extends State<PushReceiver> {
  String _text = '123';
  late AndroidNotificationChannel channel;
  late FlutterLocalNotificationsPlugin notificationsPlugin;

  void onButtonClick() {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) {
      late String infoText;
      if (message != null) {
        print('received message: $message');
        infoText = message.toString();
        Navigator.pushNamed(context, '/message',
            arguments: MessageArguments(message, true));
      } else {
        infoText = 'no message found';
      }
      setState(() {
        _text = '${DateTime.now()}: $infoText';
      });
    });

  }

  void resetText() {
    setState(() {
      _text = '';
    });
    print('111');
  }

  void init() async {
    channel = const AndroidNotificationChannel(
      'zlab_channel_1',
      'zlab channel#1',
      'this is zlab channel',
      importance: Importance.high,
    );

    notificationsPlugin = FlutterLocalNotificationsPlugin();

    await notificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);
  }

  void initFirebase() async {
    FirebaseApp app = await Firebase.initializeApp();
    setState(() {
      _text = '${DateTime.now()}: $app initialized';
    });
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    const String vapidKey = 'AAAAAxqG5jo:APA91bFelmsv5-Qdl1IAouPc33fYOuN7u4KQZctj3XSAsDN89uMitgqj7LoyAs6VB_H6lcDjnUiFVB7mPCJts_yB4C_iZzt8coxEOmyJaF8GbBqCN8Ws1UBP5QWsLlZ922hqQ7KOyEPA';
    String? token = await messaging.getToken(vapidKey: vapidKey);
    print(token);
    setState(() {
      _text = '${DateTime.now()}: token is: $token';
    });
    messaging.subscribeToTopic('main_topic');
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print('its alive!!!');
      print('title: ${event.notification?.title}, text: ${event.notification?.body}');
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        OutlinedButton(
            onPressed: initFirebase,
            child: Text('init firebase'),
        ),
        OutlinedButton(
          child: const Text('reset'),
          onPressed: resetText,
        ),
        OutlinedButton(
          child: const Text('click me'),
          onPressed: onButtonClick,
        ),
        Text(_text),
      ],
    );
  }
}
